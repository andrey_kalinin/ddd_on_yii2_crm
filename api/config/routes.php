<?php

$rouresDirectory = __DIR__ . '/routes';

$files = \yii\helpers\FileHelper::findFiles($rouresDirectory);

foreach ((array)$files as $file) {
    $routes[] = require($file);
}

return $routes ?? [];