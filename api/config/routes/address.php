<?php

return
        [
            'class'         => 'yii\rest\UrlRule',
            'controller'    => [
                'address',
            ],
            'pluralize'     => false,
];
