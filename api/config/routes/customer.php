<?php

return
        [
            'class'         => 'yii\rest\UrlRule',
            'controller'    => [
                'customer',
            ],
            'tokens'        => [
                '{id}'        => '<id:\\d+>',
                '{elementId}' => '<elementId:\\d+>',
            ],
            'extraPatterns' => [
                'GET {id}/addresses'            => 'viewAddresses',
                'GET {id}/user-role'            => 'viewUserRole',
                'GET {id}/product-request'      => 'viewProductRequests',
                'GET {id}/access-specification' => 'viewAccessSpecification',

                'POST {id}/address/{elementId}'         => 'createRelationCustomer2Address',
                'POST {id}/user-role/{elementId}'       => 'createRelationCustomer2UserRole',
                'POST {id}/product-request/{elementId}' => 'createRelationManufacturer2CompanyState',

                'DELETE {id}/address/{elementId}'         => 'deleteRelationCustomer2Address',
                'DELETE {id}/user-role/{elementId}'       => 'deleteRelationCustomer2UserRole',
                'DELETE {id}/product-request/{elementId}' => 'deleteRelationManufacturer2CompanyState',
            ],
            'pluralize'     => false,
];
