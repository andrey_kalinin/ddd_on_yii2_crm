<?php

return
        [
            'class'         => 'yii\rest\UrlRule',
            'controller'    => [
                'seller',
            ],
            'tokens'        => [
                '{id}'        => '<id:\\d+>',
                '{elementId}' => '<elementId:\\d+>',
            ],
            'extraPatterns' => [
                'GET {id}/addresses'       => 'viewAddresses',
                'GET {id}/company-role'    => 'viewCompanyRole',
                'GET {id}/company-state'   => 'viewCompanyState',
                'GET {id}/employees'       => 'viewEmployees',
                'GET {id}/manufacturers'   => 'viewManufacturers',
                'GET {id}/productRequests' => 'viewProductRequests',

                'POST {id}/address/{elementId}'       => 'createRelationSeller2Address',
                'POST {id}/manufacturer/{elementId}'  => 'createRelationSeller2Manufacturer',
                'POST {id}/company-role/{elementId}'  => 'createRelationSeller2CompanyRole',
                'POST {id}/company-state/{elementId}' => 'createRelationSeller2CompanyState',
                'POST {id}/employee/{elementId}'      => 'createRelationSeller2Employee',

                'DELETE {id}/address/{elementId}'       => 'deleteRelationSeller2Address',
                'DELETE {id}/manufacturer/{elementId}'  => 'deleteRelationSeller2Manufacturer',
                'DELETE {id}/company-role/{elementId}'  => 'deleteRelationSeller2CompanyRole',
                'DELETE {id}/company-state/{elementId}' => 'deleteRelationSeller2CompanyState',
                'DELETE {id}/employee/{elementId}'      => 'deleteRelationSeller2Employee',
            ],
            'pluralize'     => false,
];
