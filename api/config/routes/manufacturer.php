<?php

return
        [
            'class'         => 'yii\rest\UrlRule',
            'controller'    => [
                'manufacturer',
            ],
            'tokens'        => [
                '{id}'        => '<id:\\d+>',
                '{elementId}' => '<elementId:\\d+>',
            ],
            'extraPatterns' => [
                'GET {id}/addresses'       => 'viewAddresses',
                'GET {id}/company-role'    => 'viewCompanyRole',
                'GET {id}/company-state'   => 'viewCompanyState',
                'GET {id}/employees'       => 'viewEmployees',
                'GET {id}/sellers'       => 'viewSellers',
                'GET {id}/price-categories'   => 'viewPriceCategories',
                'GET {id}/components' => 'viewComponents',
                'GET {id}/product-materials' => 'viewProductMaterials',
                'GET {id}/material-collections' => 'viewMaterialCollections',
                'GET {id}/product-models' => 'viewProductModels',

                'POST {id}/address/{elementId}'       => 'createRelationManufacturer2Address',
                'POST {id}/company-role/{elementId}'  => 'createRelationManufacturer2CompanyRole',
                'POST {id}/company-state/{elementId}' => 'createRelationManufacturer2CompanyState',
                'POST {id}/employee/{elementId}'      => 'createRelationManufacturer2Employee',

                'DELETE {id}/address/{elementId}'       => 'deleteRelationManufacturer2Address',
                'DELETE {id}/company-role/{elementId}'  => 'deleteRelationManufacturer2CompanyRole',
                'DELETE {id}/company-state/{elementId}' => 'deleteRelationManufacturer2CompanyState',
                'DELETE {id}/employee/{elementId}'      => 'deleteRelationManufacturer2Employee',
            ],
            'pluralize'     => false,
];