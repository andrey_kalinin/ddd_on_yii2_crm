<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use common\extendedStdComponents\crm\customer as CustomerAction;
use commonprj\components\crm\entities\customer\Customer;


/**
 * Class CustomerController
 * @package api\controllers
 */
class CustomerController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = Customer::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'viewProductRequests' => [
                'class'       => CustomerAction\ViewProductRequestsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationCustomer2Address' => [
                'class'       => CustomerAction\CreateRelationCustomer2AddressAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationCustomer2Address' => [
                'class'       => CustomerAction\DeleteRelationCustomer2AddressAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewAddresses' => [
                'class'       => CustomerAction\ViewAddressesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewAccessSpecification' => [
                'class'       => CustomerAction\ViewAccessSpecificationAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationCustomer2UserRole' => [
                'class'       => CustomerAction\CreateRelationCustomer2UserRoleAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationCustomer2UserRole' => [
                'class'       => CustomerAction\DeleteRelationCustomer2UserRoleAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewUserRole' => [
                'class'       => CustomerAction\ViewUserRoleAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}