<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use common\extendedStdComponents\crm\company as CompanyAction;
use commonprj\components\crm\entities\company\Company;


/**
 * Class CompanyController
 * @package api\controllers
 */
class CompanyController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = Company::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'createRelationCompany2Address' => [
                'class'       => CompanyAction\CreateRelationCompany2AddressAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationCompany2Address' => [
                'class'       => CompanyAction\DeleteRelationCompany2AddressAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewAddresses' => [
                'class'       => CompanyAction\ViewAddressesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationCompany2CompanyRole' => [
                'class'       => CompanyAction\CreateRelationCompany2CompanyRoleAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationCompany2CompanyRole' => [
                'class'       => CompanyAction\DeleteRelationCompany2CompanyRoleAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewCompanyRole' => [
                'class'       => CompanyAction\ViewCompanyRoleAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationCompany2CompanyState' => [
                'class'       => CompanyAction\CreateRelationCompany2CompanyStateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationCompany2CompanyState' => [
                'class'       => CompanyAction\DeleteRelationCompany2CompanyStateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewCompanyState' => [
                'class'       => CompanyAction\ViewCompanyStateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewEmployees' => [
                'class'       => CompanyAction\ViewEmployeesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationCompany2Employee' => [
                'class'       => CompanyAction\CreateRelationCompany2EmployeeAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationCompany2Employee' => [
                'class'       => CompanyAction\DeleteRelationCompany2EmployeeAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementClasses' => [
                'class'       => CompanyAction\ViewElementClassesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewProperties' => [
                'class'       => CompanyAction\ViewPropertiesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPropertyValue' => [
                'class'       => CompanyAction\ViewPropertyValueAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewParent' => [
                'class'       => CompanyAction\ViewParentAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewParents' => [
                'class'       => CompanyAction\ViewParentsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewIsParent' => [
                'class'       => CompanyAction\ViewIsParentAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewChildren' => [
                'class'       => CompanyAction\ViewChildrenAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewAssociatedEntity' => [
                'class'       => CompanyAction\ViewAssociatedEntityAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewAssociatedEntities' => [
                'class'       => CompanyAction\ViewAssociatedEntitiesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationCompany2Child' => [
                'class'       => CompanyAction\CreateRelationCompany2ChildAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationCompany2ToParent' => [
                'class'       => CompanyAction\CreateRelationCompany2ToParentAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationCompany2Children' => [
                'class'       => CompanyAction\CreateRelationCompany2ChildrenAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationCompany2AssociatedEntity' => [
                'class'       => CompanyAction\CreateRelationCompany2AssociatedEntityAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationCompany2AssociatedEntities' => [
                'class'       => CompanyAction\CreateRelationCompany2AssociatedEntitiesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewDescendants' => [
                'class'       => CompanyAction\ViewDescendantsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPropertyMultiplicityData' => [
                'class'       => CompanyAction\ViewPropertyMultiplicityDataAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationCompany2Child' => [
                'class'       => CompanyAction\DeleteRelationCompany2ChildAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationCompany2AllChildren' => [
                'class'       => CompanyAction\DeleteRelationCompany2AllChildrenAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationCompany2AssociatedEntity' => [
                'class'       => CompanyAction\DeleteRelationCompany2AssociatedEntityAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationCompany2AllAssociatedEntities' => [
                'class'       => CompanyAction\DeleteRelationCompany2AllAssociatedEntitiesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewOldProperties' => [
                'class'       => CompanyAction\ViewOldPropertiesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}