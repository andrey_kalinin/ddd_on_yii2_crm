<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\crm\entities\companyRole\CompanyRole;

/**
 * Class CompanyRoleController
 * @package api\controllers
 */
class CompanyRoleController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = CompanyRole::class;

}