<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use common\extendedStdComponents\crm\manufacturer as ManufacturerAction;
use commonprj\components\crm\entities\manufacturer\Manufacturer;


/**
 * Class ManufacturerController
 * @package api\controllers
 */
class ManufacturerController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = Manufacturer::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'viewSellers' => [
                'class'       => ManufacturerAction\ViewSellersAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPriceCategories' => [
                'class'       => ManufacturerAction\ViewPriceCategoriesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewProductMaterials' => [
                'class'       => ManufacturerAction\ViewProductMaterialsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewMaterialCollections' => [
                'class'       => ManufacturerAction\ViewMaterialCollectionsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewProductModels' => [
                'class'       => ManufacturerAction\ViewProductModelsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewComponents' => [
                'class'       => ManufacturerAction\ViewComponentsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationManufacturer2Address' => [
                'class'       => ManufacturerAction\CreateRelationManufacturer2AddressAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationManufacturer2Address' => [
                'class'       => ManufacturerAction\DeleteRelationManufacturer2AddressAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewAddresses' => [
                'class'       => ManufacturerAction\ViewAddressesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationManufacturer2CompanyRole' => [
                'class'       => ManufacturerAction\CreateRelationManufacturer2CompanyRoleAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationManufacturer2CompanyRole' => [
                'class'       => ManufacturerAction\DeleteRelationManufacturer2CompanyRoleAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewCompanyRole' => [
                'class'       => ManufacturerAction\ViewCompanyRoleAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationManufacturer2CompanyState' => [
                'class'       => ManufacturerAction\CreateRelationManufacturer2CompanyStateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationManufacturer2CompanyState' => [
                'class'       => ManufacturerAction\DeleteRelationManufacturer2CompanyStateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewCompanyState' => [
                'class'       => ManufacturerAction\ViewCompanyStateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewEmployees' => [
                'class'       => ManufacturerAction\ViewEmployeesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationManufacturer2Employee' => [
                'class'       => ManufacturerAction\CreateRelationManufacturer2EmployeeAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationManufacturer2Employee' => [
                'class'       => ManufacturerAction\DeleteRelationManufacturer2EmployeeAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}