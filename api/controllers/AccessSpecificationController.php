<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\crm\entities\accessSpecification\AccessSpecification;

/**
 * Class AccessSpecificationController
 * @package api\controllers
 */
class AccessSpecificationController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = AccessSpecification::class;

}