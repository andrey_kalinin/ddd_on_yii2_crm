<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\crm\entities\companyState\CompanyState;

/**
 * Class CompanyStateController
 * @package api\controllers
 */
class CompanyStateController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = CompanyState::class;

}