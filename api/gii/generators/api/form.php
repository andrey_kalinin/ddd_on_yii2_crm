<?php

/* @var $form yii\widgets\ActiveForm */
/* @var $generator yii\gii\generators\model\Generator */

echo $form->field($generator, 'entityContext');
echo $form->field($generator, 'entityName');

echo $form->field($generator, 'generateAll')->checkbox();