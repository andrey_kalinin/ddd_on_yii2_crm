<?php
/* @var $generator api\gii\generators\api\Generator */

echo '<?php' . PHP_EOL;
?>

namespace <?= $generator->controllerNs ?>;

use common\extendedStdComponents\CommonElementActiveController;
use common\extendedStdComponents\<?= $generator->entityContext ?>\<?= lcfirst($generator->entityName) ?> as <?= ucfirst($generator->entityName) ?>Action;
use commonprj\components\<?= $generator->entityContext ?>\entities\<?= lcfirst($generator->entityName) ?>\<?= ucfirst($generator->entityName) ?>;


/**
 * Class <?= ucfirst($generator->entityName) ?>Controller
 * @package api\controllers
 */
class <?= ucfirst($generator->entityName) ?>Controller extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = <?= ucfirst($generator->entityName) ?>::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
<?php
$methods = $generator->getClassMethods('commonprj\components\\' . $generator->entityContext . '\entities\\' . lcfirst($generator->entityName) . '\\' . ucfirst($generator->entityName));
foreach ($methods as $type) {
    ?>
            '<?= $type ?>' => [
                'class'       => <?= ucfirst($generator->entityName) ?>Action\<?= ucfirst($type) ?>Action::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
<?php
}
?>
        ];
    }

}