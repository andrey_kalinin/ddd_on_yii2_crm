<?php
/* @var $generator api\gii\generators\entity\Generator */

echo "<?php\n";
?>

namespace <?= $generator->ns ?>;

use commonprj\components\core\entities\element\ElementDBRepository;

class <?= ucfirst($generator->entityName)?>DBRepository extends ElementDBRepository
{

}