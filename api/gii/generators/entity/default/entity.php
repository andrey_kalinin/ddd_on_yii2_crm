<?php

/* @var $generator api\gii\generators\entity\Generator */

echo "<?php\n";
?>

namespace <?= $generator->ns ?>;

use Yii;
use commonprj\components\core\entities\element\Element;

class <?= ucfirst($generator->entityName)?> extends Element
{
    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app-><?= lcfirst($generator->entityName)?>Repository;
    }
}
