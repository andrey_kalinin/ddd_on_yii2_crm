<?php

/* @var $generator api\gii\generators\entity\Generator */

echo "<?php\n";
?>

namespace <?= $generator->controllerNs ?>;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\<?= lcfirst($generator->entityContext)?>\entities\<?= lcfirst($generator->entityName)?>\<?= ucfirst($generator->entityName)?>;

/**
 * Class <?= ucfirst($generator->entityName)?>Controller
 * @package api\modules\<?= lcfirst($generator->entityContext)?>\controllers
 */
class <?= ucfirst($generator->entityName)?>Controller extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = <?= ucfirst($generator->entityName)?>::class;

}