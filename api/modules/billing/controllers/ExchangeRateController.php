<?php

namespace api\modules\billing\controllers;

use commonprj\services\CurrencyService;
use Yii;
use yii\rest\Controller;

/**
 * Class AddressController
 * @package api\modules\crm\controllers
 */
class ExchangeRateController extends Controller
{
    public function actionIndex()
    {
        /** @var CurrencyService $currencyService */
        $currencyService = Yii::$app->currency;

        return $currencyService->getExchangeRates();
    }

}