<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=172.17.0.1;dbname=core',
            'username' => '<core>',
            'password' => '<gfhjkm1>',
            'charset' => 'utf8',
        ],
        'dbSearch' => [
            'class'    => 'yii\db\Connection',
            'dsn'      => 'pgsql:host=172.17.0.1;dbname=search',
            'username' => '<search>',
            'password' => '<search>',
            'charset'  => 'utf8',
        ],
        'dbMedia'  => [
            'class'    => 'yii\db\Connection',
            'dsn'      => 'pgsql:host=172.17.0.1;dbname=media',
            'username' => '<media>',
            'password' => '<media>',
            'charset'  => 'utf8',
        ],
        'dbI18n'   => [
            'class'    => 'yii\db\Connection',
            'dsn'      => 'pgsql:host=172.17.0.1;dbname=i18n',
            'username' => '<i18n>',
            'password' => '<i18n>',
            'charset'  => 'utf8',
        ],
        'db_mcore' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=172.17.0.1;dbname=mcore',
            'username' => '<mcore>',
            'password' => '<mcore>',
            'charset' => 'utf8',
        ],
        'import' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=172.17.0.1;dbname=import',
            'username' => '<import>',
            'password' => '<import>',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];
