<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 1/19/2018
 * Time: 6:10 PM
 */

use commonprj\components\core\models\PropertyRecord;
//use commonprj\components\core\models\ElementCategoryRecord;
use commonprj\components\core\models\ElementClassRecord;
use commonprj\components\core\models\PropertyValueStringRecord;
use commonprj\components\core\models\PropertyValueTextRecord;
use commonprj\components\core\models\PropertyArrayRecord;
use commonprj\components\core\models\PropertyRangeRecord;
use commonprj\components\core\models\PropertyListItemRecord;
use commonprj\components\core\models\PropertyTreeItemRecord;

return [
    'source' => [
        'dbSearch' => [
            [
                'table_name' => PropertyRecord::tableName(),
                'field_name' => 'name',
                'is_string'  => true,
            ],
            [
                'table_name' => PropertyRecord::tableName(),
                'field_name' => 'description',
                'is_string'  => false,
            ],
//            [
//                'table_name' => ElementCategoryRecord::tableName(),
//                'field_name' => 'name',
//                'is_string'  => true,
//            ],
            [
                'table_name' => ElementClassRecord::tableName(),
                'field_name' => 'description',
                'is_string'  => false,
            ],


            [
                'table_name' => PropertyValueStringRecord::tableName(),
                'field_name' => 'value',
                'is_string'  => true,
            ],
            [
                'table_name' => PropertyValueTextRecord::tableName(),
                'field_name' => 'value',
                'is_string'  => false,
            ],

            [
                'table_name' => PropertyArrayRecord::tableName(),
                'field_name' => 'name',
                'is_string'  => true,
            ],
            [
                'table_name' => PropertyRangeRecord::tableName(),
                'field_name' => 'name',
                'is_string'  => true,
            ],
            [
                'table_name' => PropertyListItemRecord::tableName(),
                'field_name' => 'label',
                'is_string'  => true,
            ],
            [
                'table_name' => PropertyTreeItemRecord::tableName(),
                'field_name' => 'label',
                'is_string'  => true,
            ],


        ],
    ],

    'existingLangs' => [
        'rus-RU',       // Русский
        'eng-US',       // Английский
        'zho-ZH',       // Китайский
        'ukr-UK',       // Украинский
        'pol-PL',       // Польский
        'tur-TR',       // Турецкий
        'ita-IT',       // Итальянский
        'ger-DE',       // Немецкий
    ],

    'intlMapping' => [
        'rus' => 'Any-Latin; Latin-ASCII; [\u0100-\u7fff] remove;',
        'eng' => 'Any-Latin; Latin-ASCII; [\u0100-\u7fff] remove;',
        'zho' => 'Any-Latin; Latin-ASCII; [\u0100-\u7fff] remove;',
        'ukr' => 'Any-Latin; Latin-ASCII; [\u0100-\u7fff] remove;',
        'pol' => 'Any-Latin; Latin-ASCII; [\u0100-\u7fff] remove;',
        'tur' => 'Any-Latin; Latin-ASCII; [\u0100-\u7fff] remove;',
        'ita' => 'Any-Latin; Latin-ASCII; [\u0100-\u7fff] remove;',
        'ger' => 'Any-Latin; Latin-ASCII; [\u0100-\u7fff] remove;',
    ],

    'transliterate' => [
        'name',
    ],

    'skipTransalte' => [
        "materialGroupId",
        "materialTemplateId",
        "legalName",
        "positionId",
        "entityClass",
        "entityMethod",
        "contextId",
        "sysname",
        "fullAddress",
        "country",
        "postalCode",
        "city",
        "geoLocation",
        "organizationCode",
        "site",
        "country",
        "district",
        "street",
        "house",
        "flat",

        'vendorCode',
        'image',
        'texture',
        'colors',
        'pattern',
        'vendorCodeMpn',
        'razmer',

    ],
];
