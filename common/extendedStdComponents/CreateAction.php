<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 04.07.2016
 */

namespace common\extendedStdComponents;

use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseDBRepository;
use commonprj\extendedStdComponents\BaseAction;
use Yii;
use yii\base\Model;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;
use yii\db\ActiveRecord;

/**
 * CreateAction implements the API endpoint for creating a new model from the given data.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CreateAction extends BaseAction
{
    /**
     * @var string the scenario to be assigned to the new model before it is validated and saved.
     */
    public $scenario = Model::SCENARIO_DEFAULT;

    /**
     * @return BaseCrudModel
     * @throws HttpException
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /** @var BaseCrudModel $model */
        $model = new $this->modelClass();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $result = $model->save();


        if (!$result) {
            if ($model->hasErrors()) {
                throw new HttpException(422, json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE));
            } else {
                throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
            }
        }

        Yii::$app->response->statusCode = 201;
        Yii::$app->response->headers->add('id', $model->id);
        return null;
    }
}
