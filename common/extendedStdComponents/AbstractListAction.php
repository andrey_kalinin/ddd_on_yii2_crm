<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/27/2018
 * Time: 11:02 AM
 */

namespace common\extendedStdComponents;

use commonprj\extendedStdComponents\EntityDataProvider;
use commonprj\services\search\EntityDataFilter;
use yii\data\ActiveDataProvider;
use yii\rest\Action;
use yii\data\DataFilter;
use Yii;

class AbstractListAction extends Action
{
    /**
     * @var callable a PHP callable that will be called to prepare a data provider that
     * should return a collection of the models. If not set, [[prepareDataProvider()]] will be used instead.
     * The signature of the callable should be:
     *
     * ```php
     * function (IndexAction $action) {
     *     // $action is the action object currently running
     * }
     * ```
     *
     * The callable should return an instance of [[ActiveDataProvider]].
     *
     * If [[dataFilter]] is set the result of [[DataFilter::build()]] will be passed to the callable as a second parameter.
     * In this case the signature of the callable should be the following:
     *
     * ```php
     * function (IndexAction $action, mixed $filter) {
     *     // $action is the action object currently running
     *     // $filter the built filter condition
     * }
     * ```
     */
    public $prepareDataProvider;
    /**
     * @var DataFilter|null data filter to be used for the search filter composition.
     * You must setup this field explicitly in order to enable filter processing.
     * For example:
     *
     * ```php
     * [
     *     'class' => 'yii\data\ActiveDataFilter',
     *     'searchModel' => function () {
     *         return (new \yii\base\DynamicModel(['id' => null, 'name' => null, 'price' => null]))
     *             ->addRule('id', 'integer')
     *             ->addRule('name', 'trim')
     *             ->addRule('name', 'string')
     *             ->addRule('price', 'number');
     *     },
     * ]
     * ```
     *
     * @see DataFilter
     *
     * @since 2.0.13
     */
    public $dataFilter;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->dataFilter = [
            'class'       => EntityDataFilter::class,
            'searchModel' => function () {
                return empty($this->findModel)
                    ? $this->modelClass::instantiate(0)
                    : $this->findModel::instantiate(0);
            },
        ];
    }

    /**
     * This method is called right before `run()` is executed.
     * You may override this method to do preparation work for the action run.
     * If the method returns false, it will cancel the action.
     *
     * @return bool whether to run the action.
     */
    protected function beforeRun()
    {
        $result = parent::beforeRun();

        $controller = $this->controller;
        $controller->setSerializerParams([
            'collectionEnvelope' => 'items',
            'metaEnvelope'       => 'pager',
            'expandParam'        => 'with',
        ]);

        return $result;
    }


    /**
     * @return ActiveDataProvider
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        return $this->prepareDataProvider();
    }

    /**
     * Prepares the data provider that should return the requested collection of the models.
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider()
    {
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }

        $filter = null;
        if ($this->dataFilter !== null) {
            $this->dataFilter = Yii::createObject($this->dataFilter);
            if ($this->dataFilter->load($requestParams)) {
                $filter = $this->dataFilter->build(false);
                if ($filter === false) {
                    return $this->dataFilter;
                }
            }
        }

        if ($this->prepareDataProvider !== null) {
            return call_user_func($this->prepareDataProvider, $this, $filter);
        }

        /* @var $modelClass \yii\db\BaseActiveRecord */
        $modelClass = $this->modelClass;

        $query = $modelClass::find();

        if (!empty($filter)) {
            $query->andWhere($filter);
        }

        return Yii::createObject([
            'class'      => EntityDataProvider::className(),
            'query'      => $query,
            'pagination' => [
                'params' => $requestParams,
            ],
            'sort'       => [
                'params' => $requestParams,
            ],
        ]);
    }

}