<?php

namespace common\extendedStdComponents\crm\seller;

use commonprj\components\crm\entities\seller\Seller;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Seller * @package api\controllers
 */
class ViewProductRequestsAction extends BaseAction
{

    /**
     * @param int $id
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id): array 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Seller $model
         */
        $model = $this->findModel($id);

        return $model->findProductRequests() ?? [];
    }

}