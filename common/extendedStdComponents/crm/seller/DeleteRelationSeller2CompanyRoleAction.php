<?php

namespace common\extendedStdComponents\crm\seller;

use commonprj\components\crm\entities\seller\Seller;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Seller * @package api\controllers
 */
class DeleteRelationSeller2CompanyRoleAction extends BaseAction
{

    /**
     * @param int $id
     * @param int $elementId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id, int $elementId) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Seller $model
         */
        $model = $this->findModel($id);

        return $model->unbindCompanyRole($elementId);
    }

}