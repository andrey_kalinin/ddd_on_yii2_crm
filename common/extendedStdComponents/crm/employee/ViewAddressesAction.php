<?php

namespace common\extendedStdComponents\crm\employee;

use commonprj\components\crm\entities\employee\Employee;
use commonprj\extendedStdComponents\BaseAction;
use Yii;

/**
 * Class Employee * @package api\controllers
 */
class ViewAddressesAction extends BaseAction
{

    /**
     * @param int $id
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id): array 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Employee $model
         */
        $model = $this->findModel($id);
        return $model->getAddresses(Yii::$app->getRequest()->getQueryParam('typeId')) ?? [];
    }

}