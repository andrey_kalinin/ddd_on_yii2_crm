<?php

namespace common\extendedStdComponents\crm\customer;

use commonprj\components\crm\entities\customer\Customer;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Customer * @package api\controllers
 */
class CreateRelationCustomer2UserRoleAction extends BaseAction
{

    /**
     * @param int $id
     * @param int $elementId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id, int $elementId) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Customer $model
         */
        $model = $this->findModel($id);

        return $model->bindUserRole($elementId);
    }

}