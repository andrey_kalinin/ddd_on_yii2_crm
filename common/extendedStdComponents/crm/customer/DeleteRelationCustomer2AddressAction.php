<?php

namespace common\extendedStdComponents\crm\customer;

use commonprj\components\crm\entities\customer\Customer;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Customer * @package api\controllers
 */
class DeleteRelationCustomer2AddressAction extends BaseAction
{

    /**
     * @param int $id
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id, int $elementId): array 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Customer $model
         */
        $model = $this->findModel($id);

        return $model->unbindAddress($elementId) ?? [];
    }

}